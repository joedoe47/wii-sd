RNEEDA
Naruto Shippuden Clash of Ninja Revolution 3

1st Wiimote-Button Activator [Anarion]
283BDC62 YYYYZZZZ

2nd Wiimote-Button Activator [Anarion]
283BDD12 YYYYZZZZ

1st Classic Controller&Pro- Button Activator [Anarion]
283BDCC2 YYYYZZZZ
If you don't know the YYYYZZZZ values which are the button values, go to WiiRD Forums

Offline Infinite Health- All Players [Anarion]
C2069668 00000002
3C602710 907C0028
60000000 00000000

Offline Infinite Chakra- All Players [Anarion]
C206A444 00000002
3C602710 907E0044
60000000 00000000

Offline Infinite Guard- All Players [Anarion]
C206A9F8 00000002
3C60XXXX 907E004C
60000000 00000000
XXXX values: 0000 = no guard || 2710 = full guard

P1- Offline Guard Modifier [Anarion]
283BDC62 00000008
42000000 92000000
04AEB08E 0000XXXX
E0000000 80008000

P2- Offline Guard Modifier [Anarion]
283BDC62 00000008
42000000 92000000
04D8B0AE 0000XXXX
E0000000 80008000
For codes above: Activate= UP

P1- Offline Combo Hit Modifier [Anarion]
283BDC62 00000008
42000000 92000000
04AEB9EE 000003E7
E0000000 80008000
1st Wiimote- Activate= UP
For instant completion of combo missions.

Offline All Players- Paper Bombs Use No Chakra [Anarion]
0413B3D4 60000000

Offline All Players- Paper Bombs Increase Chakra [Anarion]
0413B3CC 7C002214

Offline All Players- Itachi's Clones Cost No Chakra [Anarion]
0406A61C 60000000

Offline All Players- Itachi's Clones Fill Chakra [Anarion]
0406A604 60000000
The two codes above may have an effect for other characters who have attacks that use chakra.

P1- Combo Hit Modifier [Anarion]
283BDC62 00001000
42000000 92000000
04AAB9CE 00000XXX
E0000000 80008000
Wiimote- Activate= MINUS

P2- Combo Hit Modifier [Anarion]
283BDC62 00000010
42000000 92000000
04D4B9EE 00000XXX
E0000000 80008000
Wiimote- Activate= PLUS
These codes are kinda useless.
XXX Values: 2C3= 707 || 065= 101 || 3E7= 999(will be shown as 909)

P1- Guard Modifier [Anarion]
283BDD12 00000800
42000000 92000000
04AAB06E 0000XXXX
E0000000 80008000

P2- Guard Modifier [Anarion]
283BDD12 00000800
42000000 92000000
04D4B08E 0000XXXX
E0000000 80008000
For the two hacks above: Wiimote- Activate= A

*~P1- Health Modifier [Anarion]
42000000 92000000
04AAB04A 0000XXXX
E0000000 80008000

*~P1- Chakra Modifier [Anarion]
42000000 92000000
04AAB066 0000XXXX
E0000000 80008000

*~P2- Health Modifier [Anarion]
42000000 92000000
04D4B06A 0000XXXX
E0000000 80008000

*~P2- Chakra Modifier [Anarion]
42000000 92000000
04D4B086 0000XXXX
E0000000 80008000

*~P1- Squadmate Health Modifier [Anarion]
42000000 92000000
04FEB08A 0000XXXX
E0000000 80008000

*~P1- Squadmate Chakra Modifier [Anarion]
42000000 92000000
04FEB0A6 0000XXXX
E0000000 80008000
XXXX values for ALL above codes:
2710= full ||| 1388= half ||| 0000= 0
0B00- used in Health = health in LNP Mode.
All values will remain infinite.
CODES WITH SYMBOLS. Symbol key below.

Rank Board Character [Anarion]
42000000 90000000
040314CC 000000XX
E0000000 80008000

Rank Board Squadmate Character [Anarion]
42000000 90000000
040314D0 000000XX
E0000000 80008000
XX Values for two codes above:
31= Anbu Kakashi _||| 11= Hinata ___||| 28= Komachi ____||| 17= Sasuke
23= Anko ________||| 1B= Hiruko ___||| 24= Kurenai _____||| 13= Shikamaru
22= Asuma _______||| 18= Itachi ____||| 04= Lee ________||| 0E= Shino
26= Baki _________||| 1D= Jiraiya ___||| 01= Naruto ______||| 09= Temari
27= Bando _______||| 21= Kabuto ___||| 05= Neji ________||| 06= Tenten
2C= Chiyo _______||| 29= Kagura ___||| 10= No Character _||| 2A= Towa
12= Choji ________||| 03= Kakashi __||| 20= Orochimaru __||| 1F= Tsunade
1A= Deidara ______||| 30= Kakuzu __||| 2B= Rogue Ninja __||| 16= Yamato
08= Gaara ________||| 0B= Kankuro _||| 15= Sai _________||| 25= Yugao
07= Guy _________||| 0F= Kiba _____||| 02= Sakura ______|||
2F= Hidan ________||| 19= Kisame __||| 1C= Sasori ______|||

~Character Selection Timer Modifier [Anarion]
283BDC62 00006000
42000000 90000000
04051A36 0000XXXX
E0000000 80008000

~Timer Modifier [Anarion]
283BDC62 00006000
42000000 90000000
0405C21C 000XXXXX
E0000000 80008000
For the two timer codes above:
Wiimote- Activate= C & Z
60 x seconds you want = decimal ~&gt; hex = X
Some X values:
00708= 30 sec ||| 00E10= 60 sec(default) ||| 01734= 99 sec ||| All 0= draw offline/forced DC online ||| 13560= 999 sec ||| 0001E= 1/2 sec ||| 0002D= 3/4 sec

~!Infinite Time [Anarion]
283BDC62 00002400
42000000 90000000
0405C218 FFFFFF88
E0000000 80008000
Wiimote- Activate= Z & B

~!Offline Time Increase [Anarion]
283BDC62 00000200
040599C4 38830001
E0000000 80008000
283BDC62 00000100
040599C4 3883FFFF
E0000000 80008000
Wiimote- Activate= 1 || Deactivate= 2

~!Offline Time Stop [Anarion]
283BDC62 00000000
040599C8 60000000
E0000000 80008000
283BDC62 00000000
040599C8 909C003C
E0000000 80008000
Wiimote- Activate= 1 || Deactivate= 2

~Offline Instant 0 [Anarion]
283BDC62 00006200
040599C4 60000000
E0000000 80008000
282BDC62 00006800
040599C4 3883FFFF
E0000000 80008000
Wiimote- Activate= C & Z & 1 || Deactivate= C & Z & A
Ends match instantly.

P1- Moon Jump [Anarion]
283BDC62 00008000
42000000 92000000
04AAB144 3D0XXXXX
E0000000 80008000
Wiimote- Press HOME
X= floating value you want
0FFFF= reasonable floating value

~P1- Single-Battle Points Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402ABF8 0000XXXX
E0000000 80008000

~P2- Single-Battle Points Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402AC28 0000XXXX
E0000000 80008000

~P1- Single-Win Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402ABE0 0000XXXX
E0000000 80008000

~P2- Single-Win Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402AC10 0000XXXX
E0000000 80008000

~P1- Single-Loss Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402ABE8 0000XXXX
E0000000 80008000

~P2- Single-Loss Modifier [Anarion]
283BDC62 0000ZZZZ
42000000 90000000
0402AC18 0000XXXX
E0000000 80008000
ZZZZ= button value.
XXXX= # of points, wins, losses you want in hex. These codes are currently in a BETA stage. Also, they must be button activated to avoid a freeze and MUST be activated on the score board after a match before the win/loss scores are updated.
KEY
* Depending on the time these codes are used, the server MAY disconnect both players. So, try to use with caution.
~ It is highly recommended that these codes should be used with a button activator to avoid possible server D/C. For the GameCube controller button activator, look at the top line of "wiiztec"s "Final Valley Music" code.
! Once these codes have been activated, deactivation will start in the next match.
ALL Time codes must be used as separate codes otherwise they will not work.

Reserved Spot on Top 10 [Anarion]
283BDD12 YYYYZZZZ
C216D0A0 00000002
3FA07FFF 63BD01XX
93BF0010 00000000
E0000000 80008000
YYYY: NOT value || ZZZZ: button values. I can't list them all here. Find them elsewhere.
XX: Any hex value. If you don't know hex, it won't work. I recommend FF.
To activate, have at least straight 5 wins or 2000 battle points on either single or two-man squad. Use the code and win once more. Your spot will be lost if you update your score without the code.

BETA Squad Name Changer [Anarion]
42000000 90000000
040314D4 XXXXXXXX
040314D8 XXXXXXXX
040314DC XXXXXXXX
040314E0 XXXXXXXX
E0000000 80008000
X Values: GO TO- http://www.mikezilla.com/exp0012.html
Type in ASCII field. Click "encode".
Each Xs line holds a max of 4 characters. When encoded on the site provided, the values will be in pair. So do four letters/numbers at a time. Do not include the percentage symbol.
First line will be the first 4 character of the name, second line will be the other 4, etc.
Do not use characters that are not in the game. ex. %&#*:

P1- Offline Moon Jump [Anarion]
283BDC62 00000008
42000000 92000000
04KKKKKK XXXXXXXX
E0000000 80008000
P1 Wiimote- Activate= UP button
K Character Values For Player 1
B4955C= Anko ______||| B436DC= Kabuto _____||| B4A25C= Sai
B54DDC= Asuma ____||| B6005C= Kagura _____||| B49B5C= Sakura
B3A8DC= Baki ______||| B526DC= Kakashi ____||| B5AFDC= Sasori
B3F2DC= Chiyo _____||| B3B3DC= Kakuzu ____||| B3F95C= Sasuke
B799DC= Choji ______||| B38D5C= Kankuro ____||| B3685C= Shikamaru
B3CE5C= Deidara ____||| B3A15C= Kiba ______||| B3A35C= Shino
B3CA5C= Gaara _____||| B382DC= Kisame ____||| B4D45C= Temari
B3605C= Guy _______||| B4D6DC= Komachi ___||| B34DDC= Tenten
B5B8DC= Hidan _____||| B50B5C= Kurenai ____||| B550DC= Towa
B5355C= Hinata _____||| B3145C= Lee ________||| B440DC= Tsunade
B3F8DC= Hiruko _____||| B5DFDC= Naruto ____||| BB5D4DC= Yamato
B3CADC= Itachi _____||| B3925C= Neji ________||| B5E7DC= Yugao
B53DC5= Jiraiya _____||| B3E45C= Orochimaru _||| Anbu Kakashi & Bando N/A
XXXXXXXX= floating point value
41500000= highest possible value

P2- Offline Moon Jump [Anarion]
283BDD12 00000008
42000000 92000000
04KKKKKK XXXXXXXX
E0000000 80008000
P2 Wiimote- Activate= UP button
K Character Values For Player 2
DE957C= Anko ______||| DE36FC= Kabuto _____||| DEA27C= Sai
DF4DFC= Asuma ____||| E0007C= Kagura _____||| DE9B7C= Sakura
DDA8FC= Baki ______||| DF26FC= Kakashi ____||| DFAFFC= Sasori
DDF2FC= Chiyo _____||| DDB3FC= Kakuzu ____||| DDF97C= Sasuke
E199FC= Choji ______||| DD8D7C= Kankuro ____||| DD687C= Shikamaru
DDCE7C= Deidara ____||| DDA17C= Kiba ______||| DDA37C= Shino
DDCA7C= Gaara _____||| DD82FC= Kisame ____||| DED47C= Temari
DD607C= Guy _______||| DED6FC= Komachi ___||| DD4DFC= Tenten
DFB8FC= Hidan _____||| DF0B7C= Kurenai ____||| DF50FC= Towa
DF357C= Hinata _____||| DD147C= Lee ________||| DE40FC= Tsunade
DDF8FC= Hiruko _____||| DFDFFC= Naruto ____||| DFD4FC= Yamato
DDCAFC= Itachi _____||| DD927C= Neji ________||| DFE7FC= Yugao
DF3D7C= Jiraiya _____||| DDE47C= Orochimaru _||| Anbu Kakashi & Bando N/A
XXXXXXXX= floating point value
41500000= highest possible value

Offline 1st Angle Fixed Camera [Anarion]
283BDC62 00000200
04181E14 60000000
E0000000 80008000
283BDC62 00000100
04181E0C EC01002A
E0000000 80008000

Offline 2nd Angle Fixed Camera [Anarion]
283BDC62 00000200
04181E34 60000000
E0000000 80008000
283BDC62 00000100
04181E34 EC01002A
E0000000 80008000

Offline No Boundaries [Anarion]
283BDC62 00000200
04181E30 60000000
E0000000 80008000
283BDC62 00000100
04181E30 C01E0008
E0000000 80008000
You'll get out of bounds every time you jump. I recommend to activate, jump, and deactivate.

Offline Fixed Camera Angle+No Boundaries [Anarion]
283BDC62 00000200
04181E10 60000000
E0000000 80008000
283BDC62 00000100
04181E10 C01E0000
E0000000 80008000
You can only jump directly up with this but this code is the better "no boundary" code of the two. You are back in bounds when it is deactivated.
For the FOUR CODES ABOVE: Wiimote 1- Activate= 1 || Deactivate= 2

Offline- Itachi Instant Teleport [Anarion]
283BDC62 00001000
42000000 92000000
04B3CACC C16A10ED
E0000000 80008000
283BDC62 00000010
42000000 92000000
04B3CACC 415D07EF
E0000000 80008000
1st Wiimote- Activate= MINUS & PLUS buttons.
Teleport code for every character may come if I ever feel like getting to it again.

Offline- Load Full Chakra Effect [Anarion]
0406A44C 60000000
Loads the visual effect that occurs when youland a paper bomb. Effect occurs with every punch. Just eye candy.

Offline All Players- Guard Break Chakra Modifier [Anarion]
0406A628 38A0XXXX
XXXX: 2710= full chakra || 1D4C= 75% || 1388= half || 9C4= 25% || 0000= default
Rather than losing all chakra when guard is broken, this code will allow the user to have any amount of chakra after guard is broken.

Single- Score Attack Battle Modifier [Anarion]
42000000 80000000
04395AC0 00000XXX
E0000000 80008000
X Values= # of battles won
3E6= 999 (highest value)

Single- Score Attack Point Modifier [Anarion]
42000000 80000000
04395AB8 00XXXXXX
E0000000 80008000
X Values= # of points
98967F= 9999999 (highest value)

Always have 999,999 ryo [wiiztec]
42000000 90000000
04027144 000F423F
E0000000 80008000

Final valley music switcher toggle Z+left(dpad) on GCC [wiiztec]
283BDC30 00000011
077E96E0 0000003C
62676D5F 7374675F
7368756D 61747375
5F65785F 6578312E
62727374 6D006267
6D5F7374 675F7368
756D6174 73755F67
635F6763 342E6272
73746D00 00000000
CC000000 00000000
077E96E0 0000003C
62676D5F 7374675F
7368756D 61747375
5F67635F 6763342E
62727374 6D006267
6D5F7374 675F7368
756D6174 73755F65
785F6578 312E6272
73746D00 00000000
E0000000 80008000
switches the music that normally plays in the final valley stage for the music that plays when the match is Naruto vs Sasuke (is on by default)

P1 infnite health all offline modes [wiiztec]
42000000 92000000
04AEB068 00002710
E0000000 80008000

P1 infinite chakra all offline modes [wiiztec]
42000000 92000000
04AEB084 00002710
E0000000 80008000

P2 infinite health all offline modes [wiiztec]
42000000 92000000
04D8B08A 00002710
E0000000 80008000

P2 infinite chakra all offline modes [wiiztec]
42000000 92000000
04D8B0A4 00002710
E0000000 80008000

P3/P1's squadmate infinite health all offline modes [wiiztec]
42000000 92000000
0502B0A8 00002710
E0000000 80008000

P3/P1's squadmate infinite chakra all offline modes [wiiztec]
42000000 92000000
0502B0C4 00002710
E0000000 80008000

P4/P2's squadmate infinite health all offline modes [wiiztec]
42000000 92000000
052CB0C8 00002710
E0000000 80008000

P4/P2's squadmate infinite chakra all offline modes [wiiztec]
42000000 92000000
052CB0E4 00002710
E0000000 80008000

Force d/c from WiFi (L+R+Z on GCN) [hetoan2]
283BDC30 00000070
42000000 92000000
04D4B068 00000000
04D4B070 00002710
E0000000 80008000
does not count as a loss or win